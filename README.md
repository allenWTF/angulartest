# README #

### How do I get set up? ###

To start development server run these commands. The server will start on port *:3000.

```
npm install
npm start
```

To start unit tests run these command.

```
karma start
```