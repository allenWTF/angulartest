let express = require('express');
var app = express();
var server = app.listen(3000);
var rp = require('request-promise');
var Promise = require("bluebird");


app.use(express.static('C:/temp'));

var topsites = [{
    name: 'Facebook',
    link: 'https://facebook.com/',
    img: 'https://daytube.az/assets_new/images/icons/social/1.jpg'
},
{
    name: 'Netflix',
    link: 'https://www.netflix.com',
    img: 'https://yt3.ggpht.com/-9El0rLwfX5E/AAAAAAAAAAI/AAAAAAAAAAA/iVYt84ud62o/s32-c-k-no-mo-rj-c0xffffff/photo.jpg'
},
{
    name: 'Linkedin',
    link: 'https://www.linkedin.com/',
    img: 'https://static.licdn.com/sc/h/9wcfzhuisnwhyauwp7t9xixy7'
},
{
    name: 'YouTube',
    link: 'https://www.youtube.com/',
    img: 'https://www.youtube.com/yts/img/favicon_32-vflOogEID.png'
},
{
    name: 'Mera',
    link: 'http://rtfm.mera.ru/',
    img: 'http://rtfm.mera.ru/favicon.ico'
},
{
    name: 'A Simple Guide to Getting Started With Grunt',
    link: 'https://scotch.io/tutorials/a-simple-guide-to-getting-started-with-grunt',
    img: 'https://scotch.io/favicon.ico'
},
{
    name: 'npm',
    link: 'https://www.npmjs.com/',
    img: 'https://www.npmjs.com/static/images/touch-icons/favicon-32x32.png'
},
{
    name: 'AngularJS',
    link: 'https://angularjs.org/',
    img: 'https://material.angularjs.org/latest/favicon.ico'
}];

var menu = ["My Feed", "Politics", "US", "World", "Technology", "Entertainment", "Sports", "Money", "Lifestyle", "Autos", "Video"];

var sources = [{
    id: 'associated-press',
    name: 'Associated Press',
    category: 'world'
},
{
    id: 'talksport',
    name: 'TalkSport',
    category: 'sports'
},
{
    id: 'buzzfeed',
    name: 'BuzzFeed',
    category: 'entertainment'
},
{
    id: 'cnn',
    name: 'CNN',
    category: 'world'
},
{
    id: 'espn',
    name: 'ESPN',
    category: 'sports'
},
{
    id: 'four-four-two',
    name: 'FourFourTwo',
    category: 'sports'
},
{
    id: 'polygon',
    name: 'Polygon',
    category: 'entertainment'
},
{
    id: 'metro',
    name: 'Metro',
    category: 'world'
},
{
    id: 'the-telegraph',
    name: 'The Telegraph',
    category: 'politics'
},
{
    id: 'time',
    name: 'TIME',
    category: 'politics'
},
{
    id: 'usa-today',
    name: 'USA Today',
    category: 'us'
},
{
    id: 'ars-technica',
    name: 'Ars Technica',
    category: 'technology'
},
{
    id: 'techradar',
    name: 'TechRadar',
    category: 'technology'
},
{
    id: 'engadget',
    name: 'Engadget',
    category: 'technology'
},
{
    id: 'the-economist',
    name: 'The Economist',
    category: 'money'
},
{
    id: 'daily-mail',
    name: 'Daily Mail',
    category: 'lifestyle'
}

];

var clnames = ["item1x2", "item2x2"];

var news = [];

app.get('/', function (req, res) {
    res.sendFile(path.join('C:/temp/index.html'));
});

app.get('/getTopSites', function (req, res) {
    res.send(topsites);
});

app.get('/getMenu', function (req, res) {
    res.send(menu);
});

app.get('/getSources', function (req, res) {
    res.send(sources);
});


app.get('/getNews', function (req, res) {
    var sortedNews = [];
    var news_called_count = parseInt(req.query.ncc);

    if (req.query.cat === "myfeed") {
        if (news[news_called_count + 19] !== undefined) {
            for (i = news_called_count; i < news_called_count + 20; i++) {
                sortedNews.push(news[i]);
            }
            res.send({
                newsRow: sortedNews,
                ncc: news_called_count + 20,
                isDone: false
            });
        }
        else {
            for (i = news_called_count; i < news.length; i++) {
                sortedNews.push(news[i]);
            }
            res.send({
                newsRow: sortedNews,
                isDone: true
            })
        }
    }
    else {
        news.forEach(function (item) {
            if (item.category === req.query.cat) {
                sortedNews.push(item);
            }
        });

        var sortedCatNews = []

        if (sortedNews[news_called_count + 19] !== undefined) {
            for (i = news_called_count; i < news_called_count + 20; i++) {
                sortedCatNews.push(sortedNews[i]);
            }
            res.send({
                newsRow: sortedCatNews,
                ncc: news_called_count + 20,
                isDone: false
            });
        }
        else {
            for (i = news_called_count; i < sortedNews.length; i++) {
                sortedCatNews.push(sortedNews[i]);
            }
            res.send({
                newsRow: sortedCatNews,
                isDone: true
            })
        }

    }
})


function updateNews() {
    news = [];
    var allNews = [];
    var ps = [];

    //sources = shuffle(sources);

    for (var i = 0; i < sources.length; i++) {
        var singlenews = {
            uri: 'https://newsapi.org/v1/articles?source=' + sources[i].id + '&sortBy=top&apiKey=b3b24dee16af478391d10b7231e4c2bb',
            json: true
        };
        ps.push(rp(singlenews));
    }

    Promise.all(ps.map(function (promise) {
        return Promise.resolve(promise).reflect()
    })).each(function (inspection) {
        if (inspection.isFulfilled()) {
            allNews.push(inspection.value());
        } else {
            console.error("A promise in the array was rejected with", inspection.reason());
        }
    })
        .then(() => sortNews(allNews));

}

function sortNews(allNews) {

    for (i = 0; i < allNews.length; i++) {
        for (j = 0; j < allNews[i].articles.length; j++) {
            allNews[i].articles[j].source = sources[i].name;
            allNews[i].articles[j].category = sources[i].category;
            news.push(allNews[i].articles[j]);
        }
    }

    news = shuffle(news);
}

function shuffle(array) {
    let counter = array.length;

    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);

        counter--;

        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

(function my_func() {
    updateNews();
    console.log("NEWS WERE UPDATED");
    setTimeout(my_func, 60000);
})();


console.log("Express server listening on port 3000");