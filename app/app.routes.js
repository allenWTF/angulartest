app.config(function ($stateProvider, $urlRouterProvider) {
    
        $urlRouterProvider.otherwise('/myfeed');
    
        $stateProvider
    
            .state('my feed', {
                url: '/myfeed',
                templateUrl: 'views/myfeed.html',
                controller: 'news-controller'
            })
    
            .state('politics', {
                url: '/politics',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('us', {
                url: '/us',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('world', {
                url: '/world',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('technology', {
                url: '/technology',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('entertainment', {
                url: '/entertainment',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('sports', {
                url: '/sports',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('money', {
                url: '/money',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('lifestyle', {
                url: '/lifestyle',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('autos', {
                url: '/autos',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            })
    
            .state('video', {
                url: '/video',
                templateUrl: 'views/category.html',
                controller: 'news-controller',
            });
    
    });

