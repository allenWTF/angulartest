app.controller('news-controller', function ($scope, $http, $sce, $rootScope, srcService, cutService) {
	$scope.news = [];
	$scope.sources = [];
	$scope.isDone = false;
	this.cur_row_length = 0;
	this.max_row_length = 4;
	this.news_called_count = 0;
	this.cur_rows = 0;
	this.clnames = ["item1x2", "item2x2"];
	var self = this;

	$http.get('http://localhost:3000/getSources').then(
		function (response) {
			$scope.sources = response.data;
		},
		function (response) {
			console.log(response.data);
		}
	)

	$scope.getNews = function (cat) {
		var category = cat || window.location.href.substr(window.location.href.lastIndexOf('/') + 1, window.location.href.length - 1);
		$http({
			url: 'http://localhost:3000/getNews',
			method: "GET",
			params: {cat: category, ncc: self.news_called_count},
		}).then(
			function(response) {
				for (i = 0; i < response.data.newsRow.length; i++) {
						var rand = Math.floor(Math.random() * self.clnames.length);
						self.cur_row_length = (self.cur_row_length === 4 ? 0 : self.cur_row_length);
						if (self.cur_row_length + 1 === self.max_row_length) {
							response.data.newsRow[i].clName = (($scope.news.length === 3 && category === "myfeed") ? "hidden" : self.clnames[0]);
							self.cur_row_length = 0;
							self.cur_rows++;
						}
						else {
							if (category === "myfeed" && $scope.news.length < 3) {
								rand = 0;
							}
							response.data.newsRow[i].clName = self.clnames[rand];
							self.cur_row_length += rand + 1;
						}
						response.data.newsRow[i].urlToImage = srcService.trustSrc(response.data.newsRow[i].urlToImage);
						response.data.newsRow[i].title = cutService.cut(response.data.newsRow[i].title, 75);
						$scope.news.push(response.data.newsRow[i]);
				}
				self.news_called_count = response.data.ncc;
				$scope.isDone = response.data.isDone;
			}
		).catch(
		function (response) {
			console.log("Error while getting news");
			console.log(response);
		});
	}

	$scope.goToArticle = function (url) {
		window.open(url);
	}
});