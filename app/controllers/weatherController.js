app.controller('weather-controller', weatherCtrl);

function weatherCtrl(cutService, srcService, $http) {
    this.city = cutService.cut("Nizhniy Novgorod, Russian Federation", 30);
    this.forecasts = [];
    this.getWeather($http, srcService);
}

weatherCtrl.prototype.getWeather = function ($http, srcService) {

    var self = this;
    $http.get('http://api.apixu.com/v1/forecast.json?key=6409ee331ffe42edad7121329172610&q=Nizhny Novgorod&days=5').then(
        function(response) {
            self.logo = srcService.trustSrc("http:" + response.data.current.condition.icon);
            self.temperature = response.data.current.temp_c;
            self.condition = response.data.current.condition.text;
            self.precipitation = response.data.current.precip_mm;
            response.data.forecast.forecastday.forEach(function(element) {
                var forecast = {
                    day: self.getNameOfDay(new Date(element.date).getDay()),
                    logo: "http:" + element.day.condition.icon,
                    maxtemp: Math.round(element.day.maxtemp_c),
                    mintemp: Math.round(element.day.mintemp_c)
                }
                self.forecasts.push(forecast);    
            });
            self.updated = Math.floor((response.data.location.localtime_epoch - response.data.current.last_updated_epoch)/60);
        },
        function (response) {
             console.log(response);
        }
    )
}

weatherCtrl.prototype.getNameOfDay = function (number) {
    switch (number) {
        case 0:
            return "Mon";
        case 1:
            return "Tue";
        case 2:
            return "Wed";
        case 3:
            return "Thu";
        case 4:
            return "Fri";
        case 5:
            return "Sat";
        case 6:
            return "Sun";
    }
}

weatherCtrl.prototype.goToWeather = function() {
    window.open("https://www.apixu.com/weather/q/nizhny-novgorod-novgorod-russia-2821180?loc=2821180");
}

weatherCtrl.prototype.closeWeather = function() {
    var weather_block = document.getElementsByClassName("bottom-weather")[0];
    var hidden_item = document.getElementsByClassName("bottom-hidden")[0];
    weather_block.classList.toggle("deleteWeather");
    setTimeout(function() {
        weather_block.classList.toggle("bottom-hidden");
        hidden_item.classList.toggle("bottom-item1x2");
        hidden_item.classList.remove("bottom-hidden");
        hidden_item.classList.toggle("putNews");
    }, 1500);
    hidden_item.getElementsByClassName("bottom-hidden-picture")[0].classList.toggle("bottom-item1x2-picture");
    hidden_item.getElementsByClassName("bottom-hidden-category")[0].classList.toggle("bottom-item1x2-category");
    hidden_item.getElementsByClassName("bottom-hidden-h1")[0].classList.toggle("bottom-item1x2-h1");
    hidden_item.getElementsByClassName("bottom-hidden-source")[0].classList.toggle("bottom-item1x2-source");
}

