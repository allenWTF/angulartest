app.controller('topsites-controller', function ($scope, $http, cutService, srcService) {
    
        $http.get('http://localhost:3000/getTopSites').then(
            function (response) {
                $scope.topsites = response.data;
                $scope.topsites.forEach(function(item) {
                    item.name = cutService.cut(item.name, 15);
                    item.img = srcService.trustSrc(item.img);
                });
            },
            function (response) {
                console.log(response.data);
            }
        )
    
        $scope.goTo = function(url) {
            window.open(url);
        }
    
    });