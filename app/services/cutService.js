app.service('cutService', function() {
	this.cut = function(old_string, n) {
		if (old_string.length < n) {
			return old_string
		}
		else {
			var new_string = old_string.substr(0, n);
			new_string = new_string.substr(0, Math.min(new_string.length, new_string.lastIndexOf(" ")));
			new_string += "...";
			return new_string;
		}
	}
});