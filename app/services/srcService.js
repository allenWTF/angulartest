app.service('srcService', function($sce) {
	this.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
});