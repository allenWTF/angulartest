describe('Weather controller', function () {
    var weatherCtrl, scope, httpBackend, http, srcService;

    beforeEach(angular.mock.module('app'));

    beforeEach(function () {
        inject(function (_$controller_, _$rootScope_, _$httpBackend_, _$http_, _srcService_) {
            httpBackend = _$httpBackend_;
            http = _$http_;
            srcService = _srcService_;
            scope = _$rootScope_.$new();
            weatherCtrl = _$controller_('weather-controller', {
                $scope: scope,
            });
        })
    });

    it('has to return forecast for 5 days', function () {
        httpBackend.expect('GET', 'http://api.apixu.com/v1/forecast.json?key=6409ee331ffe42edad7121329172610&q=Nizhny Novgorod&days=5')
            .respond(
            {
                "location": {
                    "localtime_epoch": 1510303819
                },
                "current": {
                    "condition": {
                        "icon": '//cdn.apixu.com/weather/64x64/day/122.png',
                        "text": "Overcast",
                    },
                    "temp_c": -1.0,
                    "precip_mm": 0.0,
                    "last_updated_epoch": 1510302608
                },
                "forecast": {
                    "forecastday": [
                        {
                            "date": 2017 - 11 - 10,
                            "day": {
                                "condition": {
                                    "icon": '//cdn.apixu.com/weather/64x64/day/113.png'
                                },
                                "maxtemp_c": 2.1,
                                "mintemp_c": -3.4,
                            }
                        },
                        {
                            "date": 2017 - 11 - 10,
                            "day": {
                                "condition": {
                                    "icon": '//cdn.apixu.com/weather/64x64/day/113.png'
                                },
                                "maxtemp_c": 2.1,
                                "mintemp_c": -3.4,
                            }
                        },
                        {
                            "date": 2017 - 11 - 10,
                            "day": {
                                "condition": {
                                    "icon": '//cdn.apixu.com/weather/64x64/day/113.png'
                                },
                                "maxtemp_c": 2.1,
                                "mintemp_c": -3.4,
                            }
                        },
                        {
                            "date": 2017 - 11 - 10,
                            "day": {
                                "condition": {
                                    "icon": '//cdn.apixu.com/weather/64x64/day/113.png'
                                },
                                "maxtemp_c": 2.1,
                                "mintemp_c": -3.4,
                            }
                        },
                        {
                            "date": 2017 - 11 - 10,
                            "day": {
                                "condition": {
                                    "icon": '//cdn.apixu.com/weather/64x64/day/113.png'
                                },
                                "maxtemp_c": 2.1,
                                "mintemp_c": -3.4,
                            }
                        }
                    ]
                }
            }
            );

            httpBackend.flush();

           expect(weatherCtrl.forecasts.length).toEqual(5);
           for (i = 0; i < 5; i++) {
               expect(weatherCtrl.forecasts[i].day).toBeDefined();
               expect(weatherCtrl.forecasts[i].logo).toBeDefined();
               expect(weatherCtrl.forecasts[i].maxtemp).toBeDefined();
               expect(weatherCtrl.forecasts[i].mintemp).toBeDefined();
           }
    });

});