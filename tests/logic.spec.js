describe('Cut service', function () {
    var cutService;

    beforeEach(angular.mock.module('app'));

    beforeEach(function () {
        inject(function (_cutService_) {
            cutService = _cutService_;
        })
    });

    it('should generate a string that is shorter than 75 symbols', function () {
        var test_string = "This is the test string that is way way way way way way way way way way way way way way way longer than 75 symbols.";
        var new_string = cutService.cut(test_string, 75);

        expect(new_string.length).not.toBeGreaterThan(75);
    });

});

describe('Src service', function () {
    var srcService;

    beforeEach(angular.mock.module('app'));

    beforeEach(function () {
        inject(function (_srcService_) {
            srcService = _srcService_;
        })
    });

    it('should return trusted url', function () {
        var test_url = "https://daytube.az/assets_new/images/icons/social/1.jpg";
        var trusted_url = srcService.trustSrc(test_url);

        expect(trusted_url.$$unwrapTrustedValue).toBeDefined();
    });

});

describe('News controller', function () {
    var newsCtrl, scope, httpBackend;

    beforeEach(angular.mock.module('app'));


    beforeEach(function () {
        inject(function (_$rootScope_, _$httpBackend_) {
            httpBackend = _$httpBackend_;
            scope = _$rootScope_.$new();
        })

        httpBackend.expect('GET', 'http://localhost:3000/getSources')
            .respond(
            [{
                id: 'associated-press',
                name: 'Associated Press',
                category: 'world'
            },
            {
                id: 'bbc-sport',
                name: 'BBC Sport',
                category: 'sports'
            },
            {
                id: 'buzzfeed',
                name: 'BuzzFeed',
                category: 'entertainment'
            },
            {
                id: 'cnn',
                name: 'CNN',
                category: 'world'
            },
            {
                id: 'espn',
                name: 'ESPN',
                category: 'sports'
            },
            {
                id: 'four-four-two',
                name: 'FourFourTwo',
                category: 'sports'
            },
            {
                id: 'ign',
                name: 'IGN',
                category: 'entertainment'
            },
            {
                id: 'metro',
                name: 'Metro',
                category: 'world'
            },
            {
                id: 'the-telegraph',
                name: 'The Telegraph',
                category: 'politics'
            },
            {
                id: 'time',
                name: 'TIME',
                category: 'politics'
            }]
            );
    });

    describe('after first call of getNews() with myfeed category it should return 20 news array and $scope.isDone should be false. Also first 3 news should have 1x2 sizes so that weather block will fit in perfectly', function () {
        beforeEach(function () {
            inject(function (_$controller_) {
                newsCtrl = _$controller_('news-controller', {
                    $scope: scope,
                });
                //spyOn(scope, 'getNews').and.callThrough();
            })
        });
        it('category = myfeed, news_called_count = 0, array of news on server have enough items', function () {

            httpBackend.expect('GET', 'http://localhost:3000/getNews?cat=myfeed&ncc=0')
                .respond({
                    newsRow: [
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        }
                    ],
                    isDone: false,
                    ncc: 20
                });

            var category = "myfeed";
            scope.getNews(category);
            httpBackend.flush();

            expect(scope.news.length).toEqual(20);
            expect(scope.isDone).toBe(false);
            expect(newsCtrl.news_called_count).toEqual(20);
            for (i = 0; i < 3; i++) {
                expect(scope.news[i].clName).toEqual("item1x2");
            }
        });
    });
    describe('last call of getNews() with myfeed category should return all remaining news (e.g. 5) array and $scope.isDone should be true', function () {
        beforeEach(function () {
            inject(function (_$controller_) {
                newsCtrl = _$controller_('news-controller', {
                    $scope: scope,
                });
                newsCtrl.news_called_count = 95;
            })
        });

        it('category = myfeed, news_called_count = 95, array of news on server doesnt have enough items', function () {
            httpBackend.expect('GET', 'http://localhost:3000/getNews?cat=myfeed&ncc=95')
                .respond({
                    newsRow: [
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        },
                        {
                            author: 'Charlie Campbell / Beijing',
                            title: 'President Trump Says China Could ‘Easily’ Rein in North Korea',
                            description: '',
                            url: 'http://time.com/5016617/donald-trump-china-north-korea-2/',
                            urlToImage: 'https://timedotcom.files.wordpress.com/2017/11/gettyimages-871875554.jpg?quality=85',
                            publishedAt: '2017-11-09T01:15:17Z',
                            source: 'TIME',
                            category: 'politics'
                        }
                    ],
                    isDone: true
                });

            var category = "myfeed";
            scope.getNews(category);
            httpBackend.flush();

            expect(scope.news.length).toEqual(5);
            expect(scope.isDone).toBe(true);
            expect(newsCtrl.news_called_count).toBe(undefined);
        });
    });
});