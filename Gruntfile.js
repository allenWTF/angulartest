module.exports = function (grunt) {

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  grunt.initConfig({

    // get the configuration info from package.json ----------------------------
    // this way we can use things like name and version (pkg.name)
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      dist: {
        src: [
          'assets/libs/jquery-3.2.1.js',
          'assets/libs/angular.min.js',
          'node_modules/angular-translate/dist/angular-translate.js',
          'assets/libs/angular-ui-router.min.js',
          'assets/libs/ng-infinite-scroll.min.js',
          'assets/libs/stickyfill.min.js',
          'app/**/*.js',
          'logic.js',
          'weather.js'
        ],
        dest: 'assets/js/core.js',
      }
    },

    watch: {
      scripts: {
        files: ['logic.js', 'weather.js', 'app/**/*.js'],
        tasks: ['concat']
      },
      styles: {
        files: ['assets/styles/styles.css', 'assets/styles/styles.less'],
        tasks: ['less']
      }
    },

    less: {
      build: {
        files: {
          'assets/styles/styles.css': 'assets/styles/styles.less'
        }
      }
    }
    // all of our configuration will go here

  });

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // we can only load these if they are in our package.json
  // make sure you have run npm install so our app can find these
  //grunt.loadNpmTasks('grunt-contrib-jshint');
  //grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  //grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');


};

